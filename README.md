## Node.JS Konami Lz77 Compressor, Decompressor
konami lz77 compress based on node.js

# How to use
```js
var Lz77 = require('node-klz77');
var compressed = Lz77.compress(Buffer.from("Hello, World!"));
// return <Buffer ff 48 65 6c 6c 6f 2c 20 57 1f 6f 72 6c 64 21 00 00>

var decompressed = Lz77.decompress(compressed);
// return <Buffer 48 65 6c 6c 6f 2c 20 57 6f 72 6c 64 21>

var decompressedString = decompressed.toString('utf8');
// return Hello, World!
```